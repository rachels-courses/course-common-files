# Topic Review: Arrays

## Terminology

Arrays are a way to collect a series of variables under one name.

It is important to understand the terminology of arrays:

* **Element** - One of the variables within an array
* **Index** - The integer position of the element within the array
* **Subscript Operator [ ]** - You put the index between [ and ] to access a specific element.

So if we had an array of states:

<table>
<tr>
<th>Index</th>
<td>0</td>
<td>1</td>
<td>2</td>
</tr>
<tr>
<th>Value</th>
<td>"Missouri"</td>
<td>"Kansas"</td>
<td>"Nebraska"</td>
</tr>
</table>

* This array is size 3. Its valid indices are 0, 1, and 2.
* Then the first **element** in the array begins at **index 0**.
* The value of the element at position 0 is "Missouri".
* The value of the element at position 1 is "Kansas".
* The value of the element at position 2 is "Nebraska".

## Declaring an array

For an array, all the elements of the array will be of the same data type. You cannot inter-mix data types of elements of an array in C++.

So, if you declare an array as a **string**, all of its elements will be strings.

**Must know size:** You must also know the size of your array at compile-time; you cannot
set the array size, or resize an array while the program is running. (Not with these arrays, anyway.)

```c++
string names[5];

names[0] = "Avinash";
names[1] = "Maja";
names[2] = "Nandita";
names[3] = "Su";
names[4] = "Zeus";
```

**Initializer lists:** You can also initialize your array with values all in one line of code.
If you're initializing your array with an *initializer list*, you don't need to specify the
size of the array within the [ ] brackets.

```c++
string names[] = { "Avinash", "Maja", "Nandita", "Su", "Zeus" };
```

## Declaring an array bigger than it needs to be

While we're using these types of arrays (static arrays), it is common to create an array that is
*bigger* than we need it to be, so that we can store items in the array throughout the program's running time, 
such as information a user is entering.

```c++
string classes[10];
```

In your program, you will want to keep the number 10 in mind, so that you don't allow the user to
add more than the maximum amount of items, or to try to access class # 20 when the max is only 10.

However, hard-coding the value 10 in all your loops and logic makes it difficult to go back and
update the program later.

Therefore, you should store the size of your array in a **named constant**.

```c++
const int MAX_CLASSES = 10;
string classes[MAX_CLASSES];
```

This way, you can use the named constant MAX_CLASSES throughout the program and understand what that value stands for. If you just have a bunch of ```10s``` everywhere in the program, it is difficult to tell what it does without researching the code more thoroughly.

## Keeping track of the count of items in the array

If you're declaring your array bigger than it needs to be, and starting your array off empty (or otherwise not full), you will also want to create a variable to keep track of the count of items currently in the array. This would start off at 0, and be incremented each time one item is added to the array.

In the example code below, I've created a function that is responsible for adding new items to my array of courses.
When a new course is added, the ```classCount``` is incremented, but if the array is full, no changes are made.
Without this error check, the user could add 10 classes fine, but once they try to add an 11th class, the program will crash.

```c++
void AddClass( string newClass, string[] classArray, int& classCount, int MAX_CLASSES )
{
    if ( classCount < MAX_CLASSES )
    {
        classArray[ classCount ] = newClass;
        classCount++;
    }
    else
    {
        cout << "Error: class array is full!" << endl;
    }
}

int main()
{
    const int MAX_CLASSES = 10;
    int classCount = 0;

    string classes[MAX_CLASSES];
    AddClass( "CS 134", classes, classCount, MAX_CLASSES );
    AddClass( "CS 200", classes, classCount, MAX_CLASSES );
    AddClass( "CS 210", classes, classCount, MAX_CLASSES );
    AddClass( "CS 250", classes, classCount, MAX_CLASSES );
    
    cout << "I am teaching " << classCount << " classes." << endl;

    return 0;
}
```

## Multidimensional arrays

You can also declare arrays to be multidimensional. Declarations will look like:

```c++
string toDoItems[7][24]; // 2D array

string spreadsheetCells[10][10]; // 2D array

float decade[10][12][31]; // 3D array
```

Sometimes you may want to store data, such as in a To Do list, where each day of the week contains 24 time-slots for items that the user wants to do. You will need to declare your array with both the sizes specified. Then, each *element* of the array is essentially a second array.

```c++
// Monday items (Monday = 1)
toDoItems[1][7] = "Wake up";                    // 7 am
toDoItems[1][9] = "Operating Systems class";    // 9 am

toDoItems[2][8] = "Wake up";
toDoItems[2][10] = "Data Structures class";
```

## Passing arrays to functions

Normally if you pass a variable to a function without the & operator, that argument is passed *by-value*; any changes to that variable within the function are not conserved when we return from the function...

```c++
void ChangeVar( int a )
{
    a = 2;
}

int main()
{
    int a = 0;
    ChangeVar( a );
    cout << a;      // still 0.
    return 0;
}
```

Or we have the choice to pass the argument **by-reference**, which allows us to change its value:

```c++
void ChangeVar( int& a )
{
    a = 2;
}

int main()
{
    int a = 0;
    ChangeVar( a );
    cout << a;      // is now 2
    return 0;
}
```

With arrays, however, they are **automatically passed by-reference**, not by value. This means that, when an array is passed to a function, its values **CAN** be changed by the function (unless you've made it a *const* parameter!)

```c++
void InitArray( int arr[3] )
{
    arr[0] = 1;
    arr[1] = 3;
    arr[2] = 5;
}

int main()
{
    int myArr[3];
    InitArray( myArr );
    
    return 0;
}
```

---

## Common array errors

**Common error: Off-by-one** - A common error with arrays is to try to access
an item outside the bounds of the array. This is most common when you want the
*last* element of the array, and you use the *size of the array* as the index...:

```c++
cout << state[3];
```
    
For this, if our array **contains 3 values**, the valid indices are **0, 1, and 2**.

3 is **not a valid index**, because 3 would be the **fourth item of the array**.
If our array size is 3, then 4 is **OUT OF BOUNDS**, and accessing it will cause
your program to crash.


