# Topic Review: Pointers and Memory

## Pointers

When we declare a variable, that variable will be given a **memory address** and a certain amount of space in RAM to store its value. We can use pointers to point to these memory addresses in order to achieve a few new things.

**Memory address operator &:** In order to get the memory address of a variable, you need to prefix the variable name with &.

```c++
int bob;
cout << &bob; // Display address of bob.
```

**Declaring a pointer:** To declare a pointer variable, you will specify the data-type that it will point to (int, float, string, etc.), and also add an asterisk ```*``` after that data-type to specify that it is a pointer.

The ```*``` can be attached to the data-type, the variable name, or neither -- it doesn't matter. It depends on your preference.

```c++
int* ptr1;
int * ptr2;
int *ptr3;
```

Pointers are just variables, but they store memory addresses rather than values.

```c++
string student1 = "Ruslan", student2 = "Elizabeth", student3 = "Dan", student4 = "";

string * currentStudent = &student1;
```

**Dereference operator * :** To access the *value* of the variable that is being pointed to by our pointer, we need to prefix the pointer name with an asterisk:

```c++
// Get the value to display
currentStudent = &student1;
cout << "Current student: " << *currentStudent << endl;

// Set a value
currentStudent = &student4;
*currentStudent = "Casey";
```

**Pointing to an object**

If you have a variable that is a class or struct, which contains its own member functions and variables, you might want to access these members via a pointer. 

You will need to **dereference** your object pointer in order to get to its members, but due to the order of operations of these operators in C++, it will try to execute a dot operator . before it tries to execute the de-reference operator * .

Because of this, you have two ways that you can access a member of an object via pointer: De-reference within parenthesis, or use the **member-of operator ->**.

```c++
struct Point
{
      int x, y;
};

int main()
{
      Point origin;
      
      Point* ptrPoint = &origin;
      
      // Doesn't work
      ptrPoint.x = 0;
      
      // Doesn't work
      *ptrPoint.x = 0;
      
      // Does work
      (*ptrPoint).x = 0;
      
      // Does work
      ptrPoint->x = 0;
      
      return 0;
}

```

**Changing what a pointer is pointing to**

We can change what a pointer is pointing to at any time in our program. If we happen to have several pointers, if we use the assignment operator = to set one pointer to another, it only updates the address that is pointed to. Many pointers can point to the same memory address.

```c++
string cat1 = "Spawt", cat2 = "Fluffin", cat3 = "Foozy";

string* ptrBestCat = &cat1;
string* ptrCutestCat = &cat1;
string* ptrFriendliestCat = ptrBestCat;
```

Note that ```ptrBestCat``` and ```ptrCutestCat``` are assigned the memory address of ```cat1``` *directly*, and ```ptrFriendliestCat``` is assigned the address of ```cat1``` via one of the other pointers. After this code, they will all be pointing at the same cat.

If we update the value of cat1 through any of these three pointers, the value of cat1 updates, and when we dereference any pointers pointing to it, we will get that new value.

```c++
cat1 = "Spawt Jr";

cout  << ptrBestCat << "\t"           // Spawt Jr
      << ptrCutestCat << "\t"         // Spawt Jr
      << ptrFriendliestCat << endl;   // Spawt Jr
```

**pointer1 == pointer2?**

If you try to compare two pointers with the comparison operator ==, it will return **true** if both pointers are pointing to the same address, and it will return **false** if they are pointing to different addresses.

**Safety: NULL / nullptr**

As a way to make sure we're using pointers safely, it is a best practice to set the address a pointer is pointing to to **NULL** (Older C++) or **nullptr** (Newer C++). NULL is just an alias for 0, and it is a way that we can explicitly state that the pointer is not assigned to any memory addresses.

This way, we can use an if statement to see if a pointer is pointing to something (or not) prior to accessing it.

**Program crashing: Dereferencing an invalid pointer**

If your pointer is pointing to an invalid address (e.g., you didn't initialize it, or it is pointing to NULL) and you try to dereference it, your program is going to crash.

```c++
int* myPtr = nullptr;
cout << *myPtr;         // READ ACCESS VIOLATION
```

---

## Memory

Each program running has an address space, which looks like this:

![memory layout](images/memory-layout.png)

### The **Stack**

* Is optimized, and has a limited size.
* Local variables, function parameters are allocated here.
* Variables on the stack only exist while the function it belongs to is running.
* Data is automatically freed; the programmer doesn't have to deal with the memory management.
      
### The **Heap**

* Has no size restrictions; as long as you have memory you can allocate it.
* Memory is not automatically freed here; the programmer has to make sure to free the memory they allocate.
* All dynamically allocated memory must be accessed via a **pointer**.
* Good place to store large objects like arrays, classes, etc.
* Variables created on the heap are accessible anywhere in the program by passing pointers around.

And just for your own information, **text** is where the program code is stored, and **data** is where global and static variables are stored.

### Memory errors

Some types of memory errors that you may come across are:

#### Invalid memory address

This occurs when you're trying to access unallocated (or freed up) memory.

Example 1: de-referencing a pointer to an invalid address

```c++
char* unintialized;
cout << *uninitialized << endl;     // ERROR
```

Example 2: Trying to assign data at an unallocated address

```c++
int* listOfNumbers = new int[5];
// stuff
delete [] listOfNumbers;
listOfNumbers[3] = 300;             // ERROR
```

#### Memory leaks

Memory leaks occur when memory is *allocated* but never *deallocated*. This memory cannot be recovered until the computer is reset!

```c++
int main()
{
      int studentSize;
      cin >> studentSize;
      
      string* studentList = new string[ studentSize ];
      
      return 0;               // PROBLEM: Closing the program without freeing the data!!
}
```

#### Missing allocation

Missing allocation occurs when you try to free memory that has already been freed!

```c++
int main()
{
      int* myArray = new int[50];
      // stuff happens
      delete [] myArray;
      
      // more stuff happens
      delete [] myArray;            // ERROR!!
      
      return 0;               // PROBLEM: Closing the program without freeing the data!!
}
```

---

## Dynamic variables and arrays

Being able to allocate memory for variables *dynamically*, at run-time can be useful once we're building more complex structures to store data. Their use might not be immediately obvious early on in your C++ courseowkr, but knowing how to create dynamic variables and arrays is important.

To **allocate memory**, we use the **new** keyword. To **deallocate (free) memory**, we use the **delete** keyword.

For every new, you should have a delete!

**Allocating memory**

To allocate memory for a variable, you create a pointer, and initialize with the new keyword:

```c++
int* myInteger = new int;
```

To allocate memory for an array, you create a pointer, and initialize with the new keyword and give a size:

```c++
int* myArray = new int[5];
```

**Accessing data via pointers**

For dynamic variables, you still have to dereference the pointer in order to access the data.

```c++
*myInteger = 5;
```

But you do not need to do this for dynamic arrays. (Technically, arrays are pointers behind-the-scenes anyway.)

```c++
myArray[0] = 2;
myArray[1] = 5;
```

**Freeing memory**

To free a dynamic variable, you use the delete command like:

```c++
delete myInteger;
```

To free a dynamic array, you use the delete command and subscript operator like:

```c++
delete [] myArray;
```

---

## Pointers and functions

Beyond passing arguments to functions *by value*...

```c++
void PassByValue( int a ) { ... }
```

...or *by reference*...

```c++
void PassByValue( int& a ) { ... }
```

...you can also pass arguments as pointers.

```c++
void PassByPointer( int* a ) { ... }
```

You will have to keep in mind that, within the function, it's *still* a pointer and must be treated as such (i.e., de-reference it when you need the value.)

```c++
void UpdateValue( int* ptrData )
{
      *ptrData += 10;
}
```

And when you *call* the function that has a *pointer parameter*, you need to make sure that you're passing in an *address* to that function. You will either need to use the address-of operator & on a normal variable, or you can pass a pointer in as-is.

```c++
int myData;
UpdateValue( &myData );       // pass in the address
```

```c++
int* ptrToData;
UpdateValue( ptrToData );     // pass in the address that the pointer points to.
```
