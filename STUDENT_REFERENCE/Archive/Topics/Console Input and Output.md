# Topic Review: Console Input and Output

## Output with cout

If you want to display text to the screen, you will use the **cout** command.

You can display *string-literals* (string values in double-quotes), variable values, or even operations (the operation will be performed, then the result will be displayed.)

```c++
string name = "Rach";
cout << "Hi, my name is " << name << endl;
```

You can keep chaining together different things to display with the **output stream operator <<**. You can also write your cout statements over several lines, if you have a lot of data to display.

```c++
cout 	<< "Item name: " << itemName
	 	<< "Price: $" << itemPrice
		<< "Score: " << score << " out of 5" << endl;
```

### Formatting text

**New lines**

Sometimes you might want to add new lines to your output. There are a couple of ways you can do this: With **endl**, or **\n**.

```c++
cout << "Statement 1" << endl << "Statement 2" << endl;
```

```c++
cout << "Statement 1 \nStatement 2 \n";
```

Display:

	Statement 1
	Statement 2

With **endl**, you have to close any string-literal and put endl outside of any double-quotes. On the other hand, **\n** is a *special character* and it can go within a string.

**Tabs**

If you want to add tabs to your output to align things, you can use the special character **\t**.

```c++
cout 	<< "TO DO LIST:" << endl
		<< "\t * Eat lunch" << endl
		<< "\t * Do homework" << endl
		<< "\t * Watch Netflix shows" << endl;
```

This will be displayed like:

	TO DO LIST:
		* Eat lunch
		* Do homework
		* Watch Netflix shows



## Input with cin

### Getting a single value (no spaces)

Without any user interaction, your program is essentially a script, just running through some pre-defined actions. Sometimes these types of programs are useful, but we will be dealing with user input a lot throughout the course.

If you want to get a single value from the user, you can use the **cin** command with the **input stream operator >>**.

```c++
float price;
cout << "Enter price: ";
cin >> price;
```

This also works with strings, but you can only enter one word. If you put a space after some text, it won't grab any text after the space.

```c++
string surname;
cout << "Enter surname: ";
cin >> surname;
```

With the above example, if the name contains a space ("Van Horn"), then the user would have to enter "VanHorn", otherwise only "Van" would be stored in the variable.

Keep this in mind with names. This can be a problem with building programs with forms - people don't like being told the name they've entered is invalid because it contains a space, accented characters, or is very short/long!

**Multiple inputs, one cin**

Like a cout statement, you can chain together items in a cin statement. This will allow you to catch several values at once, and store in multiple variables. Each value typed will have to be separated with a space.

```c++
int a, b, c;
cout << "Enter values for a, b, and c: ";
cin >> a >> b >> c;
```

**Common error: endl with cin** - Starting out, a common mistake is to put an endl at the end of a cin statement like this:

	cin >> myName >> endl;

This is invalid and won't compile - *endl* is a way for us to *display* new lines in our *output*, and this is not a valid command to use with *cin*.

### Getting a line of text (with spaces)

If you want the user to enter a line of text, or a name, you will need to use the **getline** function. This still uses **cin**, but in a different way.

```c++
string todo;

cout << "Enter a to-do list item: ";
getline( cin, todo );

cout << "Item: " << todo << endl;
```

Here, the user will be able to enter a string of any length, and the input ends at ENTER.

	Enter a to-do list item: Eat tacos for lunch
	Item: Eat tacos for lunch

**Error: Mixing getline and >>**

If you're switching between **getline( cin, ...)** and **cin >> ...** statements in your program, you might notice that it starts skipping certain inputs!

The way to get around this, is to put a **cin.ignore()** statement *after* each usage of **cin >> ...**. You only need to do this if you're interchanging them!

```c++
cout << "Enter title: ";
getline( cin, title );

cout << "Enter price: ";
cin >> price;
cin.ignore();
```

### Validating user input (integers)

In your programs, it is good to make sure that a user cannot enter invalid input. You can use a *while loop* to keep asking the user to re-enter their input, so long as their input is invalid.

This can be easiest to do if you're entering numeric values, with some minimum and maximum values. Let's say that we've stored a min and max in two variables, and the user has to enter something in-between:

```c++
int min = 1;
int max = 3;

cout << "1. Play" << endl;
cout << "2. Load" << endl;
cout << "3. Exit" << endl;

int choice;
cout << ">> ";
cin >> choice;

// Input validation
while ( choice < min || choice > max )
{
	// Ask the user to re-enter their input
	// if they entered something invalid.
	cout << "INVALID INPUT, try again: ";
	cin >> choice;
}

// choice is valid here.
```

The while loop is like an if statement, except that it will continue returning to the inner code while that *conditional value* ( choice < min || choice > max ) returns *true*.  If you only used an if statement for the input validation, it would only verify it one time. If the user entered something invalid the 2nd time, the program wouldn't run the validation code again, and it would continue with invalid input.

---


### User interface good practices

It can be hard to read a text-only console program. Even if it is just boring text on the screen, you should make sure your user interface is easy to read and navigate. Assume that someone else will be using your program and needs to understand what they're doing when they use it.

**Label your variables**

When displaying a variable to the screen, you should add a label to it, otherwise the user won't know why some random number showed up on the screen.

Bad:

```c++
cout << amountOfCats << endl;
```

Good:

```c++
cout << "Cat count: " << amountOfCats << endl;
```

Also notice that I've put a *SPACE* between : and the end of my string. This is so that I'll get nice output:

	Cat count: 30		(with space)

	Cat count:30		(without space)


**Prompt user for input**

When your program is expecting user input, make sure to display a message letting them know that you want input, and what kind of input to enter.

If you don't display a message, it will seem like your program has just stopped for no reason!

Bad:

```c++
cin >> amountOfCats;
```

Good:

```c++
cout << "Please enter the amount of cats you want: ";
cin >> amountOfCats;
```

Output:
	
	Please enter the amount of cats you want: (user input goes here)

Notice that I've also put a space between the : and " again, and no *endl*. When the user enters their input, it will show up at the end of my "Please enter the ... " line.


**Add new lines between program portions**

If your program has several things it does, or several menus it goes between, make sure to separate them out visually. The simplest way to do this is just with several *endl*s displayed, or you could draw a horizontal line.

Bad output:

	Choose item:
	1. add
	2. subtract
	>> 1
	enter first number: 2
	enter second number: 3
	result: 5
	Choose item:
	1. add
	2. subtract

All of that is pushed together and hard to read. Instead, it should be like:

	Choose item:
	1. add
	2. subtract
	>> 1

	-------------------------

	enter first number: 2
	enter second number: 3
	result: 5

	-------------------------

	Choose item:
	1. add
	2. subtract


You can even give the program sections headers to differentiate them...

	MAIN MENU

	Choose item:
	1. add
	2. subtract
	>> 1

	-------------------------
	ADD NUMBERS

	enter first number: 2
	enter second number: 3
	result: 5

	-------------------------
	MAIN MENU

	Choose item:
	1. add
	2. subtract


