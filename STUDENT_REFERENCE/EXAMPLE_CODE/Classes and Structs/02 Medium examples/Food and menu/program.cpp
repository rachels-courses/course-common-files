#include <iostream>
using namespace std;

#include "RestaurantClasses.hpp"

void ClearScreen();
void Pause();

int main()
{
	Food* order[10];
	int orderCount = 0;

	Menu menu;

	bool done = false;

	while (!done)
	{
		ClearScreen();
		menu.Display();

		cout << endl << "Total items ordered: " << orderCount << endl;

		cout << endl << endl << "Order (B, L, D then the #), or Q to quit." << endl;
		cout << ">> ";
		
		// GET B, L, or T
		char type;
		cin >> type;
		type = toupper(type);

		if (type == 'Q')
		{
			// quit
			break;
		}

		// GET #
		int index;
		cin >> index;

		order[orderCount] = menu.GetItem(type, index);
		orderCount++;
	}

	// Calculate total
	float totalPrice = 0;
	cout << endl << endl << "RECEIPT" << endl;
	for (int i = 0; i < orderCount; i++)
	{
		cout << "Item " << i << ": \t $" << order[i]->price << "\t" << order[i]->name << endl;

		totalPrice += order[i]->price;
	}

	cout << endl << "Total order amount: $" << totalPrice << endl << endl;

	Pause();

	return 0;
}

void ClearScreen()
{
	#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
		system("cls");
	#else
		system("clear");
	#endif
}

void Pause()
{
	#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
		system("pause");
	#else
		cout << endl << " Press ENTER to continue..." << endl;
		cin.ignore(std::numeric_limits <std::streamsize> ::max(), '\n');
	#endif
}
