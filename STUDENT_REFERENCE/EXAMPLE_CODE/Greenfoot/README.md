# Example Greenfoot games

To download a zip with all of the files, go to
[the main page of this repository](https://github.com/Rachels-Courses/Course-Common-Files)
and click **Clone or download**, then **Download ZIP**.

## Arrays and map

![Screenshot](_images/ArraysAndMap.png)

## Beesteroids

![Screenshot](_images/Beesteroids.png)

## Breakout

![Screenshot](_images/Breakout.png)

## Cow Battle

![Screenshot](_images/CowBattle.png)

## Flerpy Berd

![Screenshot](_images/FlerpyBerd.png)

## Hunter of Ducks

![Screenshot](_images/HunterOfDucks.png)

## Jumpy Jump

![Screenshot](_images/JumpyJump.png)

## LameFighter

![Screenshot](_images/LameFighter.png)

## Moose Invaders

![Screenshot](_images/MooseInvaders.png)

## Tower Monkey

![Screenshot](_images/TowerMonkey.png)
