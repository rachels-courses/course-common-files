import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MyWorld extends World
{
    public MyWorld()
    {
        super(600, 700, 1); 
        
        addObject( new Player(), 300, 350 );
    }
    
    public void act()
    {
        AddEnemy();
    }
    
    public void AddEnemy()
    {
        int number = Greenfoot.getRandomNumber( 100 );
        
        if ( number == 0 )
        {
            int x = Greenfoot.getRandomNumber( 600 );
            int y = Greenfoot.getRandomNumber( 700 );
            addObject( new Shroom(), x, y );
        }
    }
    
    public void AddBullet( int x, int y, int angle )
    {
        addObject( new Bullet( angle ), x, y );
    }
}
