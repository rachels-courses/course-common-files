import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Player extends Actor
{
    double velocity = 0;
    double acceleration = 0.1;
    int countdown = 0;
    
    int lives = 3;
    
    public void act() 
    {
        HandleKeyboard();
        Move();
        DetectCollision();
        DisplayLives();
        DetectGameOver();
        UpdateTimer();
    }
    
    public void UpdateTimer()
    {
        if ( countdown > 0 )
        {
            countdown -= 1;
        }
    }
    
    public void DetectGameOver()
    {
        if ( lives == 0 )
        {
            getWorld().showText( "Game Over", 300, 350 );
            Greenfoot.stop();
        }
    }
    
    public void DisplayLives()
    {
        getWorld().showText( "Lives: " + Integer.toString( lives ), 100, 10 );
    }
    
    public void DetectCollision()
    {
        if ( isTouching( Shroom.class ) )
        {
            lives -= 1;
        }
    }
    
    public void Move()
    {
        move( (int)velocity );
    }
    
    public void HandleKeyboard()
    {
        if ( Greenfoot.isKeyDown( "left" ) )
        {
            turn( -5 );
        }
        else if ( Greenfoot.isKeyDown( "right" ) )
        {
            turn( 5 );
        }
        
        if ( Greenfoot.isKeyDown( "space" ) && countdown <= 0 )
        {
            MyWorld world = (MyWorld)getWorld();
            world.AddBullet( getX(), getY(), getRotation() );
            countdown = 10;
        }
        
        if ( Greenfoot.isKeyDown( "up" ) )
        {
            velocity += acceleration;
        }
        else if ( Greenfoot.isKeyDown( "down" ) )
        {
            velocity -= acceleration;
        }
        else
        {
            if ( velocity > 0 )
            {
                velocity -= acceleration;
            }
            else if ( velocity < 0 )
            {
                velocity += acceleration;
            }
        }
    }
}
