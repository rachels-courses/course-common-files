import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Bullet extends Actor
{
    public Bullet( int angle )
    {
        setRotation( angle );
        DetectCollision();
    }
    
    public void act() 
    {
        move( 10 );
        
        if ( isAtEdge() )
        {
            getWorld().removeObject( this );
        }
    } 
    
    public void DetectCollision()
    {
        if ( getWorld() != null && isTouching( Shroom.class ) )
        {
            getWorld().removeObject( this );
            return;
        }
    }    
}
