import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MyWorld extends World
{
    Cat player;
    
    public MyWorld()
    {    
        super(400, 400, 1); 
        
        player = new Cat();
        addObject( player, 200, 300 );
        
        addObject( new Coin(), 300, 300 );
        addObject( new Coin(), 350, 300 );
        
        addObject( new Coin(), 100, 200 );
        addObject( new Coin(), 50, 200 );
        
        addObject( new Coin(), 325, 100 );
        addObject( new Coin(), 375, 100 );
        
        addGround();
        
        showText( "", 200, 200 );
    }
    
    public void addGround()
    {
        // Floor
        addObject( new Tile(), 20, 400 );
        addObject( new Tile(), 60, 400 );
        addObject( new Tile(), 100, 400 );
        addObject( new Tile(), 140, 400 );
        addObject( new Tile(), 180, 400 );
        addObject( new Tile(), 220, 400 );
        addObject( new Tile(), 260, 400 );
        addObject( new Tile(), 300, 400 );
        addObject( new Tile(), 340, 400 );
        addObject( new Tile(), 380, 400 );
        
        // Side block
        addObject( new Tile(), 20, 300 );
        addObject( new Tile(), 60, 300 );
        addObject( new Tile(), 100, 300 );
        
        addObject( new Tile(), 300, 200 );
        addObject( new Tile(), 340, 200 );
        addObject( new Tile(), 380, 200 );
    }
}
