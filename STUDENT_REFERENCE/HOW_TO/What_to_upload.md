# What to upload (for programming courses)

Last updated 2018-01-16

When you download starter files for programming assignments, you will notice that there are source files and a **Makefile**:

![A makefile and a source file](images/uploads-files-makefile.png)

When you upload your work, I will want **all the source files, plus the Makefile** but also **NO VISUAL STUDIO FILES**.

## Using starter code: Example

### Extracting the zip file

Here, I've downloaded a zip file with starter files.
I'm going to need to extract this zip file before I can get started.

![Zipped file](images/starter-zip.png)

You can click-and-drag the contents of the zip file to somewhere on your desktop, or you can hit "Extract All"

Here's the extracted folder:

![The folder is on the hard drive](images/extracted.png)

For this lab, it has multiple parts. Each of these folders contains one source file and one make file:

* Part 1 - Vectors
	* Makefile
	* stl_vector.cpp
* Part 2 - Lists
	* Makefile
	* stl_list.cpp
* Part 3 - Queues
	* Makefile
	* stl_queue.cpp
* Part 4 - Stacks
	* Makefile
	* stl_stack.cpp
* Part 5 - Maps
	* Makefile
	* stl_maps.cpp


Notice that there are no Visual Studio files - you will have to create your own project, and then add these files to your project.

![Files inside the folder](images/starter-projectfiles.png)


### Creating a Visual Studio project

You will keep your **Visual Studio** files separate from your **Source Files**.

In Visual Studio, create a new project. Go to **File > New > Project...**

![New project in visual studio](images/newproject.png)

On the left side, select **Visual C++** and then choose the **Empty Project**.

![Empty project is selected](images/emptyproject.png)

Give the project a name, and set the project's location. Then click **OK**.

### Importing the source file

Now you have an empty project. For this lab, which is in several parts, you will only add in **one source file at a time** as you are working on it.

Right-click the project icon in the **Solution Explorer** and go to **Add > Existing item...**

![Add existing item](images/addexisting.png)

Navigate to the source file you want to add. **It is OK if your visual studio files and the source files are in separate directories!**

![Finding the source file to add](images/add-existing-browse.png)

Once it's been added to the project, double-click on the file to open it up. You can now build and run the program, and make modifications.

### Uploading your work

Once you are done with a project, close Visual Studio.

Locate your source file on the hard-drive.

![Source files](images/starter-projectfiles.png)

You can validate that the source code is up-to-date by opening the .cpp file in notepad.

The Makefile and the stl_vector.cpp files here should be uploaded to the assignment. You can either zip both of these together...

![Compressing the files together](images/compress-files.png)

Or you can zip the folder that contains the files (IF they don't contain visual studio files)

![Zipping the folder](images/zipping-folder.png)

Then you will upload this zip file to Canvas.