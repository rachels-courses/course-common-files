# Turning in source code

When programming, usually a lot of files are generated while you do your work.
However, there are only one or a few files that are *actually important*.

When you create a project in Visual Studio or other programs, it will generate
a bunch of *project files*, but these don't contain the source code, so
if you only back up the project files, you will be left without all your actual work.

---

## What to upload?

**The only files I need in order to grade your work are the source files!**

You can either upload just the source files themselves, or you can zip
the entire project with the source files contained within.

But make sure your project folder *does* contain your source files or
you may end up losing your work.

---

## What are the source files

Depending on your operating system, your machine may or may not show file extensions.
The computers in the computer lab do **not** - but you can look at the **Type** column
to identify what files are source files.

![Windows source files](images/sourcefiles.png)

Use the table below to help you identify the source files:

<table>
<tr>
<th>Language</th>

<th>File extention</th>

<th>windows label</th>
</tr>

<tr>
<td>C++</td>
<td>.cpp, .hpp, .h</td>
<td>C++ source file, Header file</td>
</tr>

<tr>
<td>Java</td>
<td>.java</td>
<td>JAVA File</td>
</tr>

</table>

The *Microsoft Visual Studio Solution* and *VC++ Project* do not contain any code!


---

## How to turn in your source code in D2L's dropbox

You can either turn in **just the source code files**, or you can **turn in the entire project folder**.

### Turn in your source files

Locate your source files (see above).

In a visual studio project, it will usually be inside a second folder (the first folder for your project only has the project file, no source code!)

In D2L, click-and-drag your source files to the dotted box area, or click the **Upload** button there.

### Turn in your project folder

Navigate to the folder that contains your Visual Studio or Code::Blocks project. It should contain everything - make sure your source code files are also inside there!

Zip up this folder. In Windows, you can right-click the folder, and select **Send to > Compressed folder**.



![Dropbox upload](images/d2l.png)

Make sure to click **Submit to Dropbox** afterward.

---

## How to turn in your source code in GitHub (web interface)

If you want to make a new folder for your project **first**, do the following:

From the parent folder, click on the **Create new file** button.

![create a folder](images/createfolder1.png)

Type in the folder name, THEN A FORWARD-SLASH, then "readme.txt".

Typing the forward slash / turns whatever you just typed into a folder. However, you still need a *placeholder* file to go into your new folder.

![new folder](images/createfolder2.png)

Scroll to the bottom of the page and click **Commit new file**.

Once you're inside the folder that you want to store your source code within, click on the **Upload files** button.

![upload files](images/uploadgithub.png)

Click and drag your **source files** (.cpp, .hpp) to the "Drag files here" box, or click **choose your files** to select them from the file explorer.

Click on **Commit changes** when done.

![drag and drop](images/dragdrop.png)

