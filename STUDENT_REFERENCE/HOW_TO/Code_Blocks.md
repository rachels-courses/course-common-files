# How to use Code::Blocks

## Create the project

When you first open up Code::Blocks, it will have a welcome page.
You can select **Create a new project** from here, or from
**File > New > Project...**

![start page](images/codeblocks_newproject.png)


On the **New from template** window that pops up, select **Empty project** and click **Go**.

![Select Empty project](images/codeblocks_emptyproject.png)


On this screen:

![Empty project screen](images/codeblocks_newproject_dialog.png)

first click **...** and select a directory where you want to store your
project folder.

After you have done so, give your project a title - a folder will be
created for it automatically.

Then click **Next**.

The next window asks about Debug and Release options - leave these as
the defaults and click **Finish**.

![Leave the defaults, click Finish](images/codeblocks_debugrelease.png)






---

## Add files

The project is created, but will be empty and contain no source files.


### Add an empty file

Go to **File > New > Empty file**.

It will ask,

> Do you want to add this new file in the active project (has to be saved first)?

click on **Yes** and save your source file. The source file should end with
either .cpp, or .hpp.

![New file](images/codeblocks_newfile.png)

Another window pops up asking about debug and release. Leave these both checked
and click **Ok**.

![Debug and release are checked](images/codeblocks_debugagain.png)


### Add an existing file


In the **Projects** pane on the left, right-click your project file and go to **Add > Add files...**

Select an existing source file, then click **Open**.

It may ask if you want to add it to both debug and release - leave these both
checked and hit **OK**.

![Debug and release are checked](images/codeblocks_debugagain.png)


---

## Test code

You can add the following code to make sure your program will build:

```c++
#include <iostream>
using namespace std;

int main()
{
	char choice = 'n';

	while (choice != 'y')
	{

		cout << "Hello, C++!" << endl;

		cout << "Quit? (y to quit)" << endl;
		cin >> choice;
	}

	return 0;
}
```

## Build and run

To build the project, go to **Build > Build and run**, or click on the gear/play button
in the toolbar.

![Build and run](images/codeblocks_buildrun.png)

It will build and run the program. Any build errors will show up in the bottom window.

If there are no build errors, the example program will look like this:

![Example C++ program](images/codeblocks_run.png)
