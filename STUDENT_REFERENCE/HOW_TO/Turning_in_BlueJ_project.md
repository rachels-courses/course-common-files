# Turning in your BlueJ project

**Contents**

* [Locating your project folder](#locating-your-project-folder)
* [Turning in your code](#turning-in-your-code)
* [Opening your project elseware](#opening-your-project-elseware)

---

## Locating your project folder

When you create your BlueJ project, you had to create a new folder
to put your project into. This is the folder that we will need to navigate
to in our file explorer (e.g., "My computer" in Windows.)

Locate your project folder and navigate to it. It should have multiple
files in it, like this:

![A project directory with multiple files in it](images/howto_bluejproject.png)

The important files here are the **package.bluej** file, which stores
your BlueJ project information, and your **Java source code** file,
which contains all the code for your program. The other files are
unnecessary.

*(Note: in Windows, it may hide file extensions by default!
You might have to reference the "Type" column when looking for
your Java source file.)*

---

## Turning in your code

When turning in your work, you should zip up your entire project
folder and turn this in on the class project dropbox.

Exit your project folder (where you can see the files) and click on the
entire folder itself.

![Selecting the project folder](images/howto_bluejproject_folder.png)

Right-click the project folder and compress it.

* In Windows: Click on **Send to > Compressed (zipped) folder**.
* In Ubuntu Linux: Click on **Compress...**, then click **Create**.
* In Mac: Click **Compress Items**.

---

## Opening your project elseware

If you zip up your project, or download someone else's project to work with,
you will need to extract it before you can work with it.

**If you try to open the BlueJ or source code files from WITHIN the zip file, it won't work!**

To extract the file, double-click it...

* In Windows: Click on the **Extract all** button.
It will ask a path to extract to. Then hit **Extract**.
* In Ubuntu Linux: Click on the **Extract** button. ![Linux extract button](images/howto_bluejproject_linux_extract.png)
Select a directory to extract it to.
* In Mac: Double-clicking the zip file should auto-extract it.
