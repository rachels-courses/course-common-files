C++ code is standardized, so you can use the same source code in any *development environment* - whether its Visual Studio or Code::Blocks. The code files themselves are what you need.

However, the *project files* of Visual Studio can't be used in Code::Blocks and vice versa - the project files only store information about where to find your source files.

So, when going between Visual Studio and Code::Blocks, or the other way around, you will need to move our source code files (.cpp, .hpp files), and create a whole new project. Here are some steps on going from Visual Studio to Code::Blocks.

## Backing up your source code from Visual Studio

1. In the **Solution Explorer**, right-click the project file
2. Click on **Open Folder in File Explorer**
3. Locate your source file(s)
  * C++ Source (.cpp)
  * C++ Header (.hpp or .h)
4. Email yourself your source files (.cpp and .hpp)

(This is assuming you're moving computers.)

## Create a Code::Blocks Project on your machine

1. Open Code::Blocks
2. Click on **Create a new project**
3. Choose **Empty Project** and click **Go**
4. Empty project screen:
  * Use the **...** button to select a location for your project.
  * Give the project a title in the **Project title:** field.
  * Click **Next**
5. On the next screen, click **Finish** with the default settings.
6. Now we have an empty project.

## Copy over your source code

1. Open your project folder where your Code::Blocks project is stored.
2. Download the source code out of your email, and save it to this directory.

## Add source to project file

1. In the **Projects** pane of Code::Blocks (on the left), right-click your project.
2. Click on **Add files...**
3. Select your source file, and click **Open**
4. For the **Multiple Selection** screen, just click **OK**.

Now you can build and run the program!
