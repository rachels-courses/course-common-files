# Branching

---

## Condition

A **condition** is a question being asked. If statements and while loops
use these to decide whether to execute their internal code.

Conditions are generally
[**boolean expressions**](https://github.com/Rachels-Courses/Course-Common-Files/blob/organized/STUDENT_REFERENCE/GLOSSARY/03%20Boolean%20Expressions.md)
which will evaluate to either **true** or **false**.

If the condition evaluates to **true**, then the contents of the
if statement will be executed.

Example conditions:

* ```( x > 10 )``` - x is greater than 10
* ```( x > 0 && x < 10 )``` - x is between 0 and 10
* ```( name == "admin" )``` - name is "admin"
* ```( currency != '$' )``` - currency is not '$'

---

## Code block

A code block is a set of instructions stored within curly braces ```{ }```.

Functions, if statements, switch statements, while loops, and for loops
each have code blocks - which means that they have *internal code*.

The code within these code blocks are executed only in specific cases,
such as if an **if statement's condition** evaluates to **true**.

Code within a code block is generally indented forward by one tab.

---

## If statements

If statemnets are about asking *yes/no questions* to decide which set of
instructions to execute.

We can combine multiple branches together to make programs that will
handle multiple possible inputs appropriately.

There are four different ways we can work with *if statements*, which
provide us with different options for handling our inputs.

If statements work with **conditions**. These conditions can evaluate
to either **true** or **false**.

```c++
if ( CONDITION )
{
    // internal code
}
```

The condition is built from
[**boolean expressions**](https://github.com/Rachels-Courses/Course-Common-Files/blob/organized/STUDENT_REFERENCE/GLOSSARY/03%20Boolean%20Expressions.md)
and [**logic operators**](https://github.com/Rachels-Courses/Course-Common-Files/blob/organized/STUDENT_REFERENCE/GLOSSARY/02%20Operators.md)
. Questions may look like this:

* ```( x < 2 )``` - Is x less than 2?
* ```( choice == "quit" )``` - Is choice equal to "quit"?
* ```( a == b )``` - Is a and b equal?
* ```( x != 0 )``` - Is x not equal to 0?

Whenever any of these statements **evaluate to true**, then the
if statement's **internal code** is executed. The internal code is
written within opening and closing curly braces ```{ }``` following
the if statement.

#### Condition evaluation

Whether our condition results in **true** or **false** depends on
the question being asked, and the value of any variables.

For example:

```if ( x < 2 )```

* If *x* is -5, -2, 0, or 1, then the result is **true** and the contents of
the if statement would be executed.
* If *x* is 2, 3, 4, or anything larger, then the result is **false**, and
the contents of the if statement would be skipped.
    * If there are additional **else if** statements following the **if**,
    then those would be checked next.
    * If there is an **else** statement paired with the **if** statement,
    then the contents of the **else** block would be executed.
    
### If

![If statement diagram](images/ifstatement.png)

With an **if statement** on its own, we can specify some code to execute
if our if statement's condition is **true**.

If the condition evaluates to **false**, the statement is skipped entirely.

**C++:**

```c++
float numerator, denominator;
cout << "Enter numerator and denominator: ";
cin >> numerator >> denominator;

if ( denominator == 0 )
{
    cout << "Error: Cannot divide by 0!" << endl;
    return 1; // quit program
}

float result = numerator / denominator;
cout << "Result: " << result << endl;
```


### If, else

![If Else statement diagram](images/if_else_statement.png)

If a simple **If statement** isn't good enough for you, and you want to
have a **second option** that is executed in case the condition is **false**,
use an **if, else** statement.

```c++
if ( saved == false )
{
    SaveAndQuit();
}
else
{
    Quit();
}
```

Notice that the **else** statement does not have parenthesis or a conditional;
the point of else is that it will be called if the **if statement** evalutes
to false.

### If, else if

If you have multiple things you want to check for at once, you may
need multiple if statements.

But, it is a waste to use multiple if statements if you're checking
several conditions with the same variable! If only one of them can
result to **true**, there's no reason to run all the if statements
if we have already found one true one.

For example:

```c++
if ( choice == 'a' )
{
}

if ( choice == 'b' )
{
}

if ( choice == 'c' )
{
}
```

**choice** can only ever have one value! So if ```( choice == 'a' )``` evalutes
to true, then the time the program spends asking ```( choice == 'b' )?```
and ```( choice == 'c' )?``` is wasted!

Instead, we can group them into an **if, else if** block:

```c++
if ( choice == 'a' )
{
}
else if ( choice == 'b' )
{
}
else if ( choice == 'c' )
{
}
```

In this case, ```else if ( choice == 'b' )``` is only checked if
```if ( choice == 'a' )``` evaluated to **false**, and
```else if ( choice == 'c' )``` is only checked if both
```if ( choice == 'a' )``` and ```else if ( choice == 'b' )``` evaluated to **false**.


### If, else if, else

Finally, if we have multiple conditions to check, as above, but also
want to make sure to include some **default** case, we can also use an
**else** statement with our **if, else if** statements.

The **else** statement will be executed **only if all *if* and *else if* statements
evaluate to false!**

```c++
if ( choice == 'a' )
{
}
else if ( choice == 'b' )
{
}
else if ( choice == 'c' )
{
}
else
{
    cout << "invalid option!" << endl;
}
```

### Additional notes

* With each group of (if, else if, else) statements:
    * There must be exactly one **if**. There cannot be more than one if.
    * There can be 0, 1, or more amount of **else if** statements.
    * There can be 0 or 1 **else** statement. There cannot be more than one else.
* The **if** statement has a condition.
* The **else if** statement has a condition.
* The **else** has **NO** condition.

---

## Switch statements

A **switch statement** is another way we can choose between multiple
instructions to execute based on different **cases**.

Anything we write as a switch statement, we can write as an if statement,
but the reverse is not true.

For a switch statement, we are asking about one specific variable.

```switch( myVar )```

Each **case** in a switch statement corresponds to a value that
the switch variable can be **equal to**.

```c++
switch( choice )
{
    case 'a':   // choice == 'a'    is true
        // Do option A
    break;

    case 'b':   // choice == 'b'    is true
        // Do option B
    break;

    case 'c':   // choice == 'c'    is true
        // Do option C
    break;

    default:   // all the above are false
        cout << "Invalid option" << endl;
}
```

You cannot work with **relational statements** in a switch -
switch is just to check to see if some variable is equal to
several different options.

The **break;** code is used to specify when the code for some case ends.
If you do not include the **break;**, then the code will keep going!

```c++
switch( choice )
{
    case 'a':
        cout << "A";

    case 'b':
        cout << "B";
    
    break;
}
```

In this case, if the ```choice``` is equal to ```'a'```, it would
execute **case 'a'** 's code first, and then "flow through" into ```case 'b'``` 's
code next, because we don't have a **break** statement.

        choice is 'a':
        AB

        choice is 'b':
        B

Sometimes this can be by design! If you can think of a useful reason
to do this - that's fine. But just be aware that if you leave out the
**break;** then the code keeps going.
