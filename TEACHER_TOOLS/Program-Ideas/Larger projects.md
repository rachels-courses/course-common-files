Imitate a real-world application...:


**GrubHub**

* Restauraunt
* Menu
* Order
* Driver
* Customer
  
**Ticketing website (events, air travel?)**

* Event
* Ticket
* Customer
* Schedule/calendar

**Email**

* email
* contact
* spam filter

**Venmo**

* Customer/user
* Transaction
* Activity feed

**Spotify**

* User account
* Subscription/billing
* Artist
* Playlist
* Song

**Bank**

* Customer
* Account
* Transaction
* Report builder
* Money transfer

**LMS**

* People - Student, Teacher
* Course
* Assignment
* Calendar
* Discussion board
* Quiz
* lots of things...

