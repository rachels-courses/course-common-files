import os

os.system( "rm -fr results" )

rootFolder = os.path.dirname(os.path.realpath(__file__))

files = []

for ext_root, ext_dir, ext_files in os.walk( ".", topdown = False ):
	for folder in ext_dir:

		pathbase = os.path.join( ext_root, folder )

		for in_root, in_dir, in_files in os.walk( folder, topdown = False ):
		
		# directoryList.append( os.path.join( directoryList, folder* ) )

			for file in in_files:
				if ( ".hpp" in file or ".h" in file or ".cpp" in file ):
					path = os.path.join( pathbase, file )

					fileinfo = {
						"path" : path,
						"folder" : folder,
						"file" : file
					}
					
					files.append( fileinfo )

os.system( "mkdir results" )

counter = 0

for file1 in files:
	print( str( counter ) + "/" + str( len( files ) ) + ": " + file1[ "path" ] )
	counter = counter + 1
	
	for file2 in files:
		if ( file1 == file2 ):
			continue
			
		print( "\t" + file2[ "path" ] )
		
		# Create an output status
		f1 = os.path.join( file1[ "folder" ].replace( "./", "" ), file1[ "file" ] )
		f2 = os.path.join( file2[ "folder" ].replace( "./", "" ), file2[ "file" ] )
		
		filename = "results/"
		filename = filename + file1[ "folder" ] + "_" + file1[ "file" ]
		filename = filename + "-"
		filename = filename + file2[ "folder" ] + "_" + file2[ "file" ] + ".txt"

		# Diff both source files
		command = "wdiff -s '" + file1[ "path" ] + "' '" + file2[ "path" ] + "' > '" + filename + "'"
		
		#print( "File 1:  \t" + f1 )
		#print( "File 2:  \t" + f2 )
		#print( "Results: \t" + filename )
		#print( "Command: \t" + command )
		
		os.system( command )

