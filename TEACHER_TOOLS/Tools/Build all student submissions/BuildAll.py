import os

rootFolder = os.path.dirname(os.path.realpath(__file__))

directoryList = list()

for root, directories, files in os.walk( ".", topdown = False ):
	for name in directories:
		directoryList.append( os.path.join( root, name ) )

counter = 0
for folder in directoryList:
	print( str( counter ) + "...\t" + folder )
	counter = counter + 1
	os.chdir( folder )
	os.system( "g++ *.cpp > out 2>result.txt" )	
	os.chdir( rootFolder )
