import os

rootFolder = os.path.dirname(os.path.realpath(__file__))

directory_list = list()

for root, directories, files in os.walk( ".", topdown = False ):
	for name in directories:
		directory_list.append( os.path.join( root, name ) )

for folder in directory_list:
    print( folder )
    os.chdir( folder )
    os.system( "unzip *.zip -d ." )
    os.chdir( rootFolder )
