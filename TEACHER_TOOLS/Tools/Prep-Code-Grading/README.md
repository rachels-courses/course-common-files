# Prep all grading

This script will do the following:

1. Rename all the folders (based on D2L's grading scheme,
it removes the begin and end part, and leaves just the names of the students),
2. Unzip all zip files in subfolders
3. Move all code files in each student's subfolder into the "root" subfolder
(so I don't have to hunt through Visual Studio project files)
4. Run wdiff on all source files and generate a report


