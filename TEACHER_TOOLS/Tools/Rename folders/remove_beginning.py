import os

rootFolder = os.path.dirname(os.path.realpath(__file__))

directoryList = list()

for root, directories, files in os.walk( ".", topdown = False ):
    for name in directories:

        dashAt = name.find( " - " )

        strippedName = name[dashAt+3:]
        
        dashAt = strippedName.find( " - " )
        
        strippedName = strippedName[:dashAt]

        print( strippedName )

        os.system( "mv '" + name + "' '" + strippedName + "'" )
