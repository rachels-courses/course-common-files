#ifndef _STUDENT_CODE   // don't allow duplicates
#define _STUDENT_CODE   // don't allow duplicates

#include <iostream>     // use cout and cin
#include <string>       // use strings
using namespace std;    // using the C++ standard library

const string YOUR_NAME  = "enter_your_name_here";

/*
 * RULES
    * After implementing each lab, BUILD!
            If you are dealing with build errors a lot,
            then BUILD after every few lines of code you implement!
    * TEST each lab as you're working on it; don't wait until the end to test.
 * */

void LabProgram1()
{
    // Implement Program 1 here
}

void LabProgram2()
{
    // Implement Program 2 here
}

void LabProgram3()
{
    // Implement Program 3 here
}

void LabProgram4()
{
    // Implement Program 4 here
}

#endif  // don't allow duplicates
